When you open up Encryption, you will be asked to input plaintext. Type anything first to test it out. 
Then it asks for a key. This is.... the key. 

After that is completed, it will print it out and ask if you would like to send the message to someone. You can choose with a (y) or (n).

Now you have your encrypted message. If you want to unlock it, the person who you sent the message to, needs to use your decryptor. 
They will input the encrypted message, and for the key option, it needs to be what you put as the key. Or it won't work. 
Thus the title, 'key'.